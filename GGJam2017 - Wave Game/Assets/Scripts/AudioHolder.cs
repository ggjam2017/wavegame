using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHolder : MonoBehaviour {
	public static AudioHolder instance;

	public AudioClip[] jumpSounds;
	public AudioClip[] impactSounds;
	public AudioClip[] selectSounds;
	public AudioClip[] damageSounds;
	public AudioClip[] scratchSounds;
	public AudioClip[] dieSounds;
	public AudioClip[] chargeSounds;
	public AudioClip[] fullChargeSounds;
	public AudioClip menuMusic;

	void Awake() {
		//Singleton
		if (instance == null) {
			instance = this;
		} else {
			Destroy(this.gameObject);
		}
	}
}
