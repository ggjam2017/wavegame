using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
	public static CameraControl instance;

	public float shakeMagnitude = 1;
	public float shakeFrequency = 1;
	public float shakeDuration = 1;

	private bool shaking = false;

	void Awake() {
		//Singleton
		if (instance == null) {
			instance = this;
		} else {
			Destroy(this.gameObject);
		}
	}

	public void ShakeCamera() {
		if (!shaking) {
			Helper.instance.ShakeObject(this.gameObject, shakeDuration, shakeFrequency, shakeMagnitude);
			StartCoroutine("ShakeFlag");
		}
	}

	public void ShakeCamera(float duration, float frequency, float magnitude) {
		if (!shaking) {
			Helper.instance.ShakeObject(this.gameObject, duration, frequency, magnitude);
			StartCoroutine("ShakeFlag");
		}
	}

	private IEnumerator ShakeFlag() {
		shaking = true;

		yield return new WaitForSeconds(shakeDuration);

		shaking = false;
	}
}
