using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharactherControl1 : MonoBehaviour {
	public float HorizontalSpeed = 5f;
	public float JumpForce = 90f;
	public float GravityScale = 2f;
	public float WaveGravity = 10f;
	public Player Player;
	public LayerMask GroundLayer;
	public bool DisableCollisions;
	public float invincibleTime = .2f;
	[HideInInspector]
	public bool invincible = false;
	public float maxHorizontalSpeed = 7;
	public GameObject[] chargeJumpBars;
	public float minJumpHeight = .5f;
	public float maxJumpMultiplier = 2;
	public float timetoReachMaxCharge = 3;
	public GameObject chargeParticle;
	public float waveScapeDuration = .1f;
	public float waveScapeHeight = 1;

	private string _horizontalAxis;
	private string _jumpButton;
	private Rigidbody2D _rigidbody2d;
	private bool _startWave;
	private float? _lastJumpHeight;
	private Transform _groundCheck;
	private float _groundCheckRadius = 0.1f;
	private bool _isGrounded;
	private Animator anim;
	private bool facingLeft;
	private bool charging = false;
	private float _playerCollisionPushForce = 2f;
	private bool gotHit = false;
	private bool waveEscapeJump = false;

	void Awake() {
		_rigidbody2d = GetComponent<Rigidbody2D>();
		_rigidbody2d.gravityScale = GravityScale;
		_groundCheck = transform.Find("Ground Check");
		anim = GetComponent<Animator>();

		for (int i = 0; i < chargeJumpBars.Length; i++) {
			chargeJumpBars[i].SetActive(false);
		}

		chargeParticle.SetActive(false);
	}

	void OnEnable() {
		if (Player == Player.One) {
			_horizontalAxis = "L_XAxis_1";
			_jumpButton = "A_1";
		} else {
			_horizontalAxis = "L_XAxis_2";
			_jumpButton = "A_2";
		}
	}

	void FixedUpdate() {
		Collider2D groundCollider = Physics2D.OverlapCircle(_groundCheck.position, _groundCheckRadius, GroundLayer);
		_isGrounded = (groundCollider && !groundCollider.isTrigger) ? true : false;
		

		if (Input.GetAxisRaw(_horizontalAxis) != 0 && !charging && !invincible && !gotHit && !GameControl.instance.gameOver) {
			_rigidbody2d.AddForce(new Vector2(Input.GetAxisRaw(_horizontalAxis) * HorizontalSpeed, 0));
			
			if(Mathf.Abs(_rigidbody2d.velocity.x) > HorizontalSpeed){ //Limita o walk speed do jogador
				float dir = Mathf.Sign(_rigidbody2d.velocity.x);
				_rigidbody2d.velocity = new Vector2(HorizontalSpeed * dir, _rigidbody2d.velocity.y);
			}
		}

		//Limita a velocidade geral do jogador
		if (Mathf.Abs(_rigidbody2d.velocity.x) > maxHorizontalSpeed) {
			float dir = Mathf.Sign(_rigidbody2d.velocity.x);
			_rigidbody2d.velocity = new Vector2(maxHorizontalSpeed * dir, _rigidbody2d.velocity.y);
		}
	}

	void Update() {
		//Jump
		if (Input.GetButtonDown(_jumpButton) && !GameControl.instance.gameOver) {
			if (_isGrounded) {
				StartCoroutine("JumpCoroutine");

			} else if (!gotHit) { //Wave Fall
				if (!_startWave) {
					anim.SetBool("Pound", true);
					_rigidbody2d.velocity = Vector2.zero; //I like this, but it's optional =============================================
					_rigidbody2d.gravityScale = WaveGravity;
					_startWave = true;
					_lastJumpHeight = transform.position.y;
				}
			} else if (waveEscapeJump) {
				Debug.Log("foi");
				gotHit = false;
				_rigidbody2d.velocity = Vector2.zero;
				_rigidbody2d.AddForce(new Vector2(0, JumpForce * waveScapeHeight), ForceMode2D.Impulse);
			}
		}

		if (_isGrounded) {
			_rigidbody2d.gravityScale = GravityScale;
			gotHit = false;

			if (_startWave) { //Wave
				anim.SetBool("Pound", false);
				anim.SetTrigger("Impact");
				_startWave = false;
				GameControl.instance.StartWave(transform.position, (float)_lastJumpHeight - transform.position.y);
				//CameraControl.instance.shakeMagnitude = (float)_lastJumpHeight;
				_lastJumpHeight = null;
				//_rigidbody2d.AddForce (new Vector2 (0f, 10f)); //What's this?
				CameraControl.instance.ShakeCamera();
				SoundManager.instance.PlayRandomSFX(AudioHolder.instance.impactSounds);
				InvincibleStart();

				RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, Mathf.Infinity, GroundLayer);

				if (hit) {
					Instantiate(GameControl.instance.impactEfx, hit.point, Quaternion.identity);
				}

				InvincibleStart();
			}
		}

		//Animations
		FaceOtherPlayer();
		anim.SetBool("Grounded", _isGrounded);
		anim.SetFloat("VSpeed", _rigidbody2d.velocity.y);
		anim.SetFloat("HSpeed", Mathf.Abs(_rigidbody2d.velocity.x));
		anim.SetBool("Charging", charging);
		anim.SetBool("Hit", gotHit);

		if (Input.GetAxisRaw(_horizontalAxis) > 0) {
			if (facingLeft) {
				anim.SetBool("WalkingForward", false);
			} else {
				anim.SetBool("WalkingForward", true);
			}
		} else if (Input.GetAxisRaw(_horizontalAxis) < 0) {
			if (facingLeft) {
				anim.SetBool("WalkingForward", true);
			} else {
				anim.SetBool("WalkingForward", false);
			}
		}
	}

	public void FaceOtherPlayer() {
		if (Player == Player.One) {
			if (transform.position.x < GameControl.instance.player2.transform.position.x) {
				transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
				facingLeft = false;
			} else if (transform.position.x > GameControl.instance.player2.transform.position.x) {
				transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1, transform.localScale.y, transform.localScale.z);
				facingLeft = true;
			}
		} else {
			if (transform.position.x < GameControl.instance.player1.transform.position.x) {
				transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
				facingLeft = false;
			} else if (transform.position.x > GameControl.instance.player1.transform.position.x) {
				transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1, transform.localScale.y, transform.localScale.z);
				facingLeft = true;
			}
		}
	}

	public void InvincibleStart() {
		if (!invincible) {
			StartCoroutine("InvicibleCoroutine");
		}
	}

	private IEnumerator InvicibleCoroutine() {
		invincible = true;

		yield return new WaitForSeconds(invincibleTime);

		invincible = false;
		_rigidbody2d.velocity = Vector2.zero;
	}

	private IEnumerator JumpCoroutine() {
		float multiplier = minJumpHeight;
		charging = true;
		float timePassed = 0;
		chargeParticle.SetActive(true);
		AudioSource source = SoundManager.instance.PlayRandomSFX(AudioHolder.instance.chargeSounds);
		bool fullCharge = false;
		AudioSource fullChargeSource;
		float charge = 0;

		while (Input.GetButton(_jumpButton) && _isGrounded) {
			timePassed += Time.deltaTime;

			charge = Mathf.Lerp(0, 1, timePassed / timetoReachMaxCharge);
			multiplier = minJumpHeight + (charge * maxJumpMultiplier);

			//Ativa as barras de acordo com a carga 
			int barID = Mathf.FloorToInt(charge * (chargeJumpBars.Length - 1));
			chargeJumpBars[barID].SetActive(true);

			if(barID == chargeJumpBars.Length - 1 && !fullCharge){
				fullCharge = true;
				fullChargeSource = SoundManager.instance.PlayRandomSFX(AudioHolder.instance.fullChargeSounds);
				fullChargeSource.volume = .5f;
			}

			yield return null;
		}

		SoundManager.instance.SetEfxVolume(1);
		source.Stop();
		charging = false;
		_rigidbody2d.gravityScale = GravityScale;
		_rigidbody2d.AddForce(new Vector2(0, JumpForce * multiplier), ForceMode2D.Impulse);
		SoundManager.instance.PlayJumpSound(AudioHolder.instance.jumpSounds, charge);
		chargeParticle.SetActive(false);
		for (int i = 0; i < chargeJumpBars.Length; i++) {
			chargeJumpBars[i].SetActive(false);
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {

		if (collision.collider.gameObject.CompareTag("Player")) {
			Debug.Log("Collided with player");
			var otherPlayer = collision.collider.gameObject;
			if (transform.position.x <= otherPlayer.transform.position.x) {
				_rigidbody2d.AddForce(new Vector2(-1 * _playerCollisionPushForce, 0));
				otherPlayer.GetComponent<Rigidbody2D>().AddForce(new Vector2(_playerCollisionPushForce, 0), ForceMode2D.Impulse);
			} else {
				_rigidbody2d.AddForce(new Vector2(_playerCollisionPushForce, 0));
				otherPlayer.GetComponent<Rigidbody2D>().AddForce(new Vector2(-1 * _playerCollisionPushForce, 0), ForceMode2D.Impulse);
			}
		}
	}

	public void Hit() {
		SoundManager.instance.PlayRandomSFX(AudioHolder.instance.damageSounds);
		CameraControl.instance.ShakeCamera();
		gotHit = true;
		_startWave = false;
		StopCoroutine("JumpCoroutine");
		charging = false;
		for (var i = 0; i < chargeJumpBars.Length; i++) {
			var chargeJumpBar = chargeJumpBars [i];
			chargeJumpBar.SetActive (false);
		}

		StartCoroutine("WaveScape");
	}

	private IEnumerator WaveScape() {
		waveEscapeJump = true;

		yield return new WaitForSeconds(waveScapeDuration);

		waveEscapeJump = false;
	}
}



