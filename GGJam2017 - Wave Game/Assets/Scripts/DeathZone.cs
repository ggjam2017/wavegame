using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour {
	public int direction;
	public float offset = 1;
	public GameObject deathEffect;

	void OnTriggerEnter2D(Collider2D col) {
		if (col.tag == "Player" && !GameControl.instance.gameOver) {
			//Spawn Death Effect
			if (deathEffect != null) {
				Instantiate(deathEffect, col.transform.position + ((Vector3.right * direction) * offset), Quaternion.Euler(Vector3.forward * direction * -90));
			}
			//Deactivates the player
			col.gameObject.SetActive(false);

			if (col.GetComponent<CharactherControl1>().Player == Player.One) {
				GameControl.instance.SetSplash(Player.Two);
				//GameControl.instance.player2.GetComponent<CharactherControl1>().enabled = false;
			} else {
				GameControl.instance.SetSplash(Player.One);
				//GameControl.instance.player1.GetComponent<CharactherControl1>().enabled = false;
			}

			SoundManager.instance.PlayRandomSFX(AudioHolder.instance.dieSounds);
			CameraControl.instance.ShakeCamera(.2f, 10, 5);
		}
	}
}
