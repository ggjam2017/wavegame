using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {
	public static GameControl instance;

	[Header("Map")]
	public GameObject cubePrefab;
	public int noOfCubes = 25;
	public float waveMaxHeight = 10;
	public LayerMask whatIsGround;
	[Header("Game Modes")]
	public Modes[] modes;
	[Header("Musics")]
	public AudioSource[] musics;
	[Header("Misc")]
	public GameObject gambiarraGround;
	public GameObject player1;
	public GameObject player2;
	public GameObject PlayerOneWinSplash;
	public GameObject PlayerTwoWinSplash;
	public string menuSceneName;
	public SpriteRenderer bg;
	[HideInInspector]
	public bool gameOver = false;
	public float pushForce = 2;
	public GameObject impactEfx;

	private TileControl[] stageCubes;
	private float timeBetweenTiles;
	private int waveResolution = 5;
	private float waveSpeed = 1;
	private AnimationCurve currentWaveFormat;
	private float[] musicIntervals;
	private float minHeightForStepping = .8f;
	private bool _isCheckingForQuitGame;

	void Awake() {
		//Singleton
		if (instance == null) {
			instance = this;
		} else {
			Destroy(this.gameObject);
		}

		PlayerOneWinSplash.SetActive(false);
		PlayerTwoWinSplash.SetActive(false);


		musicIntervals = new float[5];
		musicIntervals[0] = 13.714f;
		musicIntervals[1] = 27.428f;
		musicIntervals[2] = 41.142f;
		musicIntervals[3] = 54.857f;
	}

	private void setPlayerBehaviourActive(bool active) {
		var playerOneControl = player1.GetComponent<CharactherControl1>();
		var playerTwoControl = player2.GetComponent<CharactherControl1>();
		playerOneControl.enabled = active;
		playerTwoControl.enabled = active;
		playerTwoControl.FaceOtherPlayer();
	}

	void Start() {

		if (player1 != null) {
			player1 = GameObject.Find("Player One");
		}
		if (player2 != null) {
			player2 = GameObject.Find ("Player Two");
		}
		setPlayerBehaviourActive (false);

		Vector3 cubeSize = new Vector3();
		stageCubes = new TileControl[noOfCubes];
		for (int i = 0; i < noOfCubes; i++) {
			stageCubes[i] = Instantiate(cubePrefab).GetComponent<TileControl>();
			cubeSize = stageCubes[i].GetComponent<BoxCollider2D>().bounds.size;
			stageCubes[i].transform.parent = this.transform;
			stageCubes[i].SetStartParameters(i, new Vector3(i * cubeSize.x, -cubeSize.y/2, 0), waveSpeed, currentWaveFormat, minHeightForStepping);
		}

		gambiarraGround.transform.localScale = new Vector3(cubeSize.x * noOfCubes, cubeSize.y, gambiarraGround.transform.localScale.z);
		gambiarraGround.transform.parent = this.transform;
		gambiarraGround.transform.localPosition = new Vector3(cubeSize.x * (noOfCubes / 2) - (cubeSize.x / 2), -cubeSize.y / 2, 0);
	}

	IEnumerator CancelQuitGameCoroutine()
	{
		var waitTime = 2f;
		yield return new WaitForSeconds (waitTime);
		_isCheckingForQuitGame = false;
		yield return null;
	}

	void Update() {
		if (Input.GetButtonDown ("Cancel")) {
			if (!_isCheckingForQuitGame) {
				_isCheckingForQuitGame = true;
				StartCoroutine (CancelQuitGameCoroutine ());
			} else {
				Application.Quit ();
			}
		}

		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			int newMode = Random.Range(1, modes.Length);

			ChangeMode(newMode);
		}
	}

	public void StartGame() {
		//timeBetweenTiles = waveSpeed / waveResolution;
		ChangeMode(0);
		SoundManager.instance.SetMusic(modes[0].music);
		setPlayerBehaviourActive (true);
		StartCoroutine("ChangeModesCoroutine");
	}

	public void StartWave(Vector3 pos, float height) {
		if (height > waveMaxHeight) {
			height = waveMaxHeight;
		}

		RaycastHit2D hit = Physics2D.Raycast(pos, -Vector2.up, Mathf.Infinity, whatIsGround);

		if (hit) {
			TileControl tile = hit.collider.GetComponent<TileControl>();
			if (tile.currentDirection == 0) {
				StartCoroutine(WaveCoroutine(tile.GetID(), 1, height));
				StartCoroutine(WaveCoroutine(tile.GetID(), -1, height));
			}
		}
	}

	private IEnumerator WaveCoroutine(int id, int dir, float height) {
		id += dir; 
		int tilesToignore = 0;

		while(id >= 0 && id < noOfCubes){
			tilesToignore--;

			if (tilesToignore <= 0 && stageCubes[id].currentDirection != 0 && stageCubes[id].currentDirection != dir) {
				height -= stageCubes[id].currentHeight;
				tilesToignore = (int)Mathf.Ceil(waveResolution / 2);
			}

			if (height > 1) {
				stageCubes[id].Jump(dir, height);
			}

			id += dir;
			yield return new WaitForSeconds(timeBetweenTiles);
		}
	}

	public void SetSplash(Player player) {
		gameOver = true;
		StartCoroutine("GameOver", player);
	}

	private IEnumerator GameOver(Player player) {
		yield return new WaitForSeconds(.6f);

		if (player == Player.One) {
			PlayerOneWinSplash.SetActive(true);
			PlayerTwoWinSplash.SetActive(false);
		} else {
			PlayerOneWinSplash.SetActive(false);
			PlayerTwoWinSplash.SetActive(true);
		}

		yield return new WaitForSeconds(.5f);

		while(true){
			if (Input.GetButtonDown("A_1") || Input.GetButtonDown("A_2")) {
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			}

			yield return null;
		}
	}

	public void ChangeMode(int newMode) {
		this.waveSpeed = modes[newMode].waveSpeed;
		this.waveResolution = modes[newMode].waveResolution;
		currentWaveFormat = modes[newMode].waveFormat;
		this.bg.sprite = modes[newMode].bg;
		this.minHeightForStepping = modes[newMode].minHeightForStepping;

		timeBetweenTiles = waveSpeed / waveResolution;

		for (int i = 0; i < noOfCubes; i++) {
			stageCubes[i].ChangeParameters(waveSpeed, currentWaveFormat, minHeightForStepping);
		}

		for (int i = 0; i < musics.Length; i++) {
			musics[i].Pause();
		}

		int randomScratch = Random.Range(0, AudioHolder.instance.scratchSounds.Length);
		SoundManager.instance.PlayRandomSFX(AudioHolder.instance.scratchSounds[randomScratch]);

		for (int i = 0; i < musics.Length; i++) {
			if (musics[i].clip == modes[newMode].music) {
				musics[i].volume = 1;
			} else {
				musics[i].volume = 0;
			}

			musics[i].UnPause();
		}

		//Debug.Log("Mode Change: " + newMode);
	}

	private IEnumerator ChangeModesCoroutine() {
		int randomMode = 0;
		int musicID = 0;

		yield return new WaitForSeconds(modes[0].music.length);
		SoundManager.instance.SetMusicVolume(0);
		for (int i = 0; i < musics.Length; i++) {
			musics[i].Play();
		}

		do {
			int newMode = Random.Range(1, modes.Length);
			while (newMode == randomMode && modes.Length > 1) {
				newMode = Random.Range(1, modes.Length);
			}

			randomMode = newMode;
			ChangeMode(randomMode);

			yield return new WaitForSeconds(musicIntervals[musicID]);

			musicID++;

			if (musicID > musics.Length) {
				musicID = 0;
			}

		} while (!gameOver);
	}
}

[System.Serializable]
public class Modes{
	public AudioClip music;
	public int waveResolution = 5;
	public float waveSpeed = 1;
	public AnimationCurve waveFormat;
	public Sprite bg;
	[Range(0, 1)]
	public float minHeightForStepping = .8f;
}
