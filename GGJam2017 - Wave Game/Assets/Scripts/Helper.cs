using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Axis{ Horizontal, Vertical };

/// <summary>
/// Class where various helpful functions can be found
/// </summary>
public class Helper : MonoBehaviour {
	public static Helper instance;
	
	private static bool hAxisDown = false;
	private static bool vAxisDown = false;

	void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy(this.gameObject);
		}
	}

	public static float GetAxisDown(Axis axis) {
		if (axis == Axis.Horizontal) {
			if (Input.GetAxisRaw(axis.ToString()) != 0) {
				if (!hAxisDown) {
					hAxisDown = true;
					return Input.GetAxisRaw(axis.ToString());
				}

				return 0;
			}

			hAxisDown = false;
			return 0;
		}

		if (axis == Axis.Vertical) {
			if (Input.GetAxisRaw(axis.ToString()) != 0) {
				if (!vAxisDown) {
					vAxisDown = true;
					return Input.GetAxisRaw(axis.ToString());
				}

				return 0;
			}

			vAxisDown = false;
			return 0;
		}

		return 0;
	}

	public void ShakeObject(GameObject obj, float shakeTime, float frequency, float magnitude) {
		StartCoroutine(Shake(obj, shakeTime, frequency, magnitude));
	}

	private IEnumerator Shake(GameObject obj, float shakeTime, float frequency, float magnitude) {
		float timePassed = 0;
		Vector3 initPos = obj.transform.localPosition;
		Vector3 newPos = new Vector3();

		while (timePassed < shakeTime) {
			timePassed += Time.deltaTime;

			newPos.x = (Mathf.PerlinNoise((Time.time * frequency), 0) - .5f) * magnitude; //Perlin noise to get smooth random values.
			newPos.y = (Mathf.PerlinNoise((Time.time * frequency), 1) - .5f) * magnitude;

			obj.transform.localPosition = initPos + newPos;
			yield return null;
		}

		obj.transform.localPosition = initPos;
	}

	public static float GetAngleBetweenVectors(Vector2 vec1, Vector2 vec2) {
		Vector2 diference = vec2 - vec1;
		float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
		return Vector2.Angle(Vector2.right, diference) * sign;
	}
}
