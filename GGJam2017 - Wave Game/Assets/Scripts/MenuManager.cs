using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {
	public GameObject menuParent;
	public GameObject creditsParent;
	public GameObject controlsParent;
	public RectTransform fadeImage;
	public RectTransform logo;
	public RectTransform pressStart;
	public RectTransform controls;
	public RectTransform credits;
	public RectTransform subt1;
	public RectTransform subt2;
	public RectTransform subt3;
	public AnimationCurve menuBeat;
	public GameObject hud;
	public float beatInterval;
	public float beatDuration;
	public float beatMagnitude;

	private bool isCredits = false;
	private bool runCredits = false;
	private bool isControls = false;
	private bool runControls = false;
	private bool waitingForChange = false;
	private Vector3 initialLogoScale;

	void Start() {
		StartCoroutine("MenuCoroutine");
		hud.SetActive(false);
	}

	private IEnumerator MenuCoroutine() {
		SoundManager.instance.SetMusic(AudioHolder.instance.menuMusic);

		while (true) {
			StartCoroutine(BumpCoroutine());

			yield return new WaitForSeconds(beatInterval);
		}
	}

	private IEnumerator BumpCoroutine() {
		float timePassed = 0;
		Vector3 logoScale = logo.localScale;
		Vector3 pressStartScale = pressStart.localScale;
		Vector3 controlsScale = controls.localScale;
		Vector3 creditsScale = credits.localScale;
		Vector3 subt1Scale = subt1.localScale;
		Vector3 subt2Scale = subt2.localScale;
		Vector3 subt3Scale = subt3.localScale;

		initialLogoScale = logoScale;

		while (timePassed < beatDuration) {
			timePassed += Time.deltaTime;

			float value = menuBeat.Evaluate(timePassed / beatDuration);

			logo.localScale = logoScale + Vector3.one * (value * beatMagnitude);
			pressStart.localScale = pressStartScale + Vector3.one * (value * beatMagnitude);
			controls.localScale = controlsScale + Vector3.one * (value * beatMagnitude);
			credits.localScale = creditsScale + Vector3.one * (value * beatMagnitude);
			subt1.localScale = subt1Scale + Vector3.one * (value * beatMagnitude);
			subt2.localScale = subt2Scale + Vector3.one * (value * beatMagnitude);
			subt3.localScale = subt3Scale + Vector3.one * (value * beatMagnitude);

			yield return null;
		}
	}

	public void GoToScene(string sceneName) {
		SceneManager.LoadScene(sceneName);
	}

	public void Exit() {
		Application.Quit();
	}

	void Update() {
		if ((Input.GetButtonDown("A_1") || Input.GetButtonDown("A_2")) && !runCredits && !runControls) {
			StopAllCoroutines();

			menuParent.gameObject.SetActive(false);
			fadeImage.gameObject.SetActive(false);

			GameControl.instance.StartGame();
			hud.SetActive(true);
			this.enabled = false;
		}

		if ((Input.GetButtonDown("B_1") || Input.GetButtonDown("B_2")) && !runControls){
			if(!runCredits){
				Debug.Log("Botão para a tela de créditos pressionado");

				if(logo.localScale == initialLogoScale){
					runCredits = true;
				}else{
					waitingForChange = true;
					isCredits = true;
				}
			}else{
				runCredits = false;
				
				menuParent.gameObject.SetActive(true);
				creditsParent.gameObject.SetActive(false);
			}
		}

		if ((Input.GetButtonDown("X_1") || Input.GetButtonDown("X_2")) && !runCredits){
			if(!runControls){
				Debug.Log("Botão para a tela de controles pressionado");

				if(logo.localScale == initialLogoScale){
					runControls = true;
				}else{
					waitingForChange = true;
					isControls = true;
				}
			}else{
				runControls = false;
				
				menuParent.gameObject.SetActive(true);
				controlsParent.gameObject.SetActive(false);
			}
		}

		if(waitingForChange && isCredits){
			if(logo.localScale == initialLogoScale){
				runCredits = true;
				waitingForChange = false;
			}
		}
		if(runCredits){
			menuParent.gameObject.SetActive(false);
			creditsParent.gameObject.SetActive(true);
		}

		if(waitingForChange && isControls){
			if(logo.localScale == initialLogoScale){
				runControls = true;
				waitingForChange = false;
			}
		}
		if(runControls){
			menuParent.gameObject.SetActive(false);
			controlsParent.gameObject.SetActive(true);
		}
	}
}
