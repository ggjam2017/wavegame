using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour {
	public static SoundManager instance;

	public int noOfChannels = 2;
	public float minPitch = .95f;
	public float maxPitch = 1.05f;
	public bool canHaveSimultaneous = false;

	private AudioSource music;
	private AudioSource[] channels;

	// Use this for initialization
	void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy(this.gameObject);
		}

		music = GetComponent<AudioSource>();

		channels = new AudioSource[noOfChannels];
		for (int i = 0; i < noOfChannels; i++) {
			channels[i] = gameObject.AddComponent<AudioSource>();
			channels[i].playOnAwake = false;
		}
	}

	void Update() {
		//Mute/Unmute all channels
		if (Input.GetKeyDown(KeyCode.M)) {
			if (!channels[0].enabled) {
				music.enabled = true;
				for (int i = 0; i < noOfChannels; i++) {
					channels[i].enabled = true;
				}
			} else {
				music.enabled = false; ;
				for (int i = 0; i < noOfChannels; i++) {
					channels[i].enabled = false;
				}
			}
		}
	}

	/// <summary>
	/// Set the volume of all/one of the EFX channels
	/// </summary>
	public void SetEfxVolume(float volume, int channel = -1) {
		if (channel > -1) {
			channels[channel].volume = volume;
		} else {
			for (int i = 0; i < noOfChannels; i++) {
				channels[i].volume = volume;
			}
		}
	}

	public void SetMusic(AudioClip clip) {
		music.clip = clip;
		music.Play();
	}

	/// <summary>
	/// Set the volume of the music
	/// </summary>
	public void SetMusicVolume(float volume) {
		music.volume = volume;
	}

	/// <summary>
	/// Set the volume of both the music and all of the EFX channels
	/// </summary>
	public void SetGlobalVolume(float volume) {
		SetMusicVolume(volume);
		SetEfxVolume(volume);
	}

	/// <summary>
	/// Plays a sound with the defined paramenters
	/// </summary>
	public void PlaySingle(AudioClip clip, bool randomPitch = false) {
		AudioSource channel;

		if (canHaveSimultaneous) { //If we can have simultaneous sounds of the same clip, that means more than one channel can play the same clip
			channel = FindFreeChannel();
		} else { //If not, then we have to check if a channel is already playing the clip
			channel = FindChannel(clip);
		}

		//If the channel was found, we play the clip
		if (channel && channel.isActiveAndEnabled) {
			channel.clip = clip;
			if (channel) {
				if (randomPitch) {
					channel.pitch = Random.Range(minPitch, maxPitch);
				} else {
					channel.pitch = 1;
				}

				channel.Play();
			}
		}
	}

	/// <summary>
	/// Plays a random sound from the passed array with random pitch
	/// </summary>
	public AudioSource PlayRandomSFX(params AudioClip[] clips) {
		//"params" lets you pass more than one parameter of the same type
		if (clips.Length > 0) {
			AudioSource channel;
			int clipID = Random.Range(0, clips.Length);

			if (canHaveSimultaneous) {
				channel = FindFreeChannel();
			} else {
				channel = FindChannel(clips);
			}

			if (channel && channel.isActiveAndEnabled) {
				channel.clip = clips[clipID];
				channel.pitch = Random.Range(minPitch, maxPitch);
				channel.Play();

				return channel;
			}
		} else {
			Debug.LogWarning("There's no sounds in the array");
		}

		return null;
	}

	/// <summary>
	/// Plays a sound based on the player's jump power
	/// </summary>
	public AudioSource PlayJumpSound(AudioClip[] clips, float jumpPower){
		//The audioclip NEEDS to have four sounds, otherwise this function will not work!
		if(clips.Length == 4){
			AudioSource channel;

			if (canHaveSimultaneous) {
				channel = FindFreeChannel();
			} else {
				channel = FindChannel(clips);
			}

			if(jumpPower < 0.25){
				channel.clip = clips[0];
				channel.Play();
			} else if(jumpPower >= 0.25 && jumpPower < 0.75){
				channel.clip = clips[1];
				channel.Play();
			} else if(jumpPower >= 0.75 && jumpPower < 1){
				channel.clip = clips[2];
				channel.Play();
			} else if(jumpPower >= 1){
				channel.clip = clips[3];
				channel.Play();
			}
			
			return channel;
		} else {
			Debug.LogWarning("This function only accepts arrays with four sounds");

			return null;
		}
	}

	/// <summary>
	/// Looks for a free channel, returning null if there's none
	/// </summary>
	private AudioSource FindFreeChannel() {
		for (int i = 0; i < noOfChannels; i++) {
			if (!channels[i].isPlaying) {
				return channels[i];
			}
		}

		Debug.Log("There's no free audio channel");
		return null;
	}

	/// <summary>
	/// Looks and returns a channel that's either free or playing the same clip passed, so we can reuse it.
	/// </summary>
	private AudioSource FindChannel(params AudioClip[] clips) {
		//If an channel is already playing the same clip, we stop it and reuse it
		for (int i = 0; i < noOfChannels; i++) {
			for (int j = 0; j < clips.Length; j++) {
				if (channels[i].isPlaying && channels[i].clip == clips[j]) {
					channels[i].Stop();
					return channels[i];
				}
			}
		}

		//If no audio channel is playing the same clip, we look for a free channel
		return FindFreeChannel();
	}
}
