using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileControl : MonoBehaviour {
    [HideInInspector]
    public int currentDirection = 0;
    [HideInInspector]
    public float currentHeight = 0;
    public float TileX { get; private set; }
	public LayerMask playerMask;
	public Color activeColor;

    private int id;
    private Vector3 initPos;
    private float jumpDuration = 0.5f;
    private AnimationCurve jumpCurve;
	private float curveTime;
	private Color defaultColor;
	private BoxCollider2D boxCollider;
	private float minHeightForStepping = .8f;

	void Awake() {
		defaultColor = GetComponent<SpriteRenderer>().color;
		boxCollider = GetComponent<BoxCollider2D>();
	}

	public void SetStartParameters(int id, Vector3 initialPos, float jumpDuration, AnimationCurve jumpCurve, float minHeightForStepping) {
		ChangeParameters(jumpDuration, jumpCurve, minHeightForStepping);

		initPos = initialPos;
		transform.localPosition = initialPos;
		this.id = id;
		gameObject.name = "Tile: " + id;
	}

	public void ChangeParameters(float jumpDuration, AnimationCurve jumpCurve, float minHeightForStepping) {
		this.jumpDuration = jumpDuration;
		this.jumpCurve = jumpCurve;
		this.minHeightForStepping = minHeightForStepping;

		transform.localPosition = new Vector3(initPos.x, initPos.y, initPos.z);
	}

    public int GetID()
    {
        return id;
    }

    public void Jump(int dir, float height)
    {
        if (currentDirection == 0)
        { //Checa se a onda tá ativa (possui direção), se não tiver, seta os valores, ativando-a
            currentDirection = dir;
            currentHeight = height;
        }

        if (height >= currentHeight)
        {
            currentHeight = height;
            currentDirection = dir;
            StopCoroutine("JumpCoroutine");
            StartCoroutine("JumpCoroutine", height);
        }
    }

    private IEnumerator JumpCoroutine() {
		GetComponent<SpriteRenderer>().color = activeColor;

        float timePassed = 0;

		boxCollider.isTrigger = true;

        while (timePassed < jumpDuration)
        {
            timePassed += Time.deltaTime;

			curveTime = timePassed / jumpDuration;
			TileX = jumpCurve.Evaluate(curveTime);
			transform.localPosition = new Vector3(initPos.x, initPos.y + (TileX * currentHeight), initPos.z);

			if (TileX >= minHeightForStepping) { //Se for maior que essa altura, o boxcollider NÃO vai ser trigger, funcionando como se fosse chão
				boxCollider.isTrigger = false;
			} else { //Se for menor, ele vai ser trigger, lançando o personagem
				boxCollider.isTrigger = true;
			}

            yield return null;
        }
        //"reseta" a onda
        currentHeight = 0;
        currentDirection = 0;
		curveTime = 0;
		GetComponent<SpriteRenderer>().color = defaultColor;
		boxCollider.isTrigger = false;
    }

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.tag == "Player") {
			CharactherControl1 player = collider.GetComponent<CharactherControl1>();
			if (!player.invincible) {
				player.Hit();
				Rigidbody2D rb = collider.GetComponent<Rigidbody2D>();
				rb.velocity = Vector3.zero;
				ForceMode2D mode = ForceMode2D.Impulse;

				if (currentDirection == 1) {
					if (curveTime <= 0.5f) {
						//Right
						rb.GetComponent<Rigidbody2D>().AddForce(new Vector2(GameControl.instance.pushForce, 1), mode);
					} else if (curveTime > 0.5f) {
						//Left
						rb.GetComponent<Rigidbody2D>().AddForce(new Vector2(-GameControl.instance.pushForce, 1), mode);
					}
				} else {
					if (curveTime <= 0.5f) {
						//Left
						rb.GetComponent<Rigidbody2D>().AddForce(new Vector2(-GameControl.instance.pushForce, 1), mode);
					} else if (curveTime > 0.5f) {
						//Right
						rb.GetComponent<Rigidbody2D>().AddForce(new Vector2(GameControl.instance.pushForce, 1), mode);
					}
				}
			}
		}
	}
}
